# -*- coding: utf-8 -*-

import requests


class RequestUtil:
    def __init__(self):
        pass

    def request(self, url, method, headers=None, param=None, content_type=None):
        """
        通用请求工具类
        :param url: 接口地址
        :param method: 接口请求方法
        :param headers: 请求头
        :param param: 参数
        :param content_type:
        :return:
        """
        try:
            if method == 'get':
                result = requests.get(url=url, params=param, headers=headers).json()
                return result
            elif method == 'post':
                if content_type == 'application/json':
                    result = requests.post(url=url, json=param, headers=headers).json()
                    return result
                else:
                    result = requests.post(url=url, data=param, headers=headers).json()
                    return result
            else:
                print("http method not allowed")
        except Exception as e:
            print("http请求报错:{0}".format(e))


if __name__ == '__main__':
    url = "https://api.xdclass.net/pub/api/v1/web/web_login"
    r = RequestUtil()
    data = {"phone":"19994334819", "pwd":"gao@367985"}
    headers = {"Content-type":"application/x-www-form-urlencoded"}
    result = r.request(url, 'post', param = data, headers = headers)
    print(result)
