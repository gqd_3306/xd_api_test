# -*- coding: utf-8 -*-

import pymysql
from warnings import filterwarnings

"""
忽略mysql的告警信息
"""
filterwarnings("ignore", category=pymysql.Warning)


class MysqlDb:

    def __init__(self):
        """
        建立数据库连接
        """
        self.conn = pymysql.connect("127.0.0.1", "root", "", "xd_api_test")
        """
        使用cursor方法获取操作游标，得到一个可以执行的sql语句，并且操作结果作为字典返回的游标
        """
        self.cur = self.conn.cursor(cursor=pymysql.cursors.DictCursor)

    def __del__(self):
        """
        关闭游标及数据库连接
        """
        self.cur.close()
        self.conn.close()

    def query(self, sql, state="all"):
        """
        查询
        :param sql:
        :param state: all默认查询全部
        :return:
        """
        self.cur.execute(sql)
        if state == "all":
            data = self.cur.fetchall()
        else:
            data = self.cur.fetchone()
        return data

    def execute(self, sql):
        """
        更新、删除、新增
        :param sql:
        :return:
        """
        try:
            """
            使用execute操作sql, commit提交事务
            """
            rows = self.cur.execute(sql)
            self.conn.commit()
            return rows
        except Exception as e:
            """
            抛出异常并回滚
            """
            print("数据库操作异常 {0}".format(e))
            self.conn.rollback()