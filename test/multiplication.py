# -*- coding: utf-8 -*-

def Multiplication():
    for i in range(1, 10):
        for j in range(1, i + 1):
            print(i, "*", j, "=", i * j, "\t", end="")
        print("")


if __name__ == '__main__':
    Multiplication()
