# -*- coding: utf-8 -*-
import unittest
from util.request_util import RequestUtil

host = "https://api.xdclass.net"


class UserTestCase(unittest.TestCase):

    def testLogin(self):
        """
        用户登录
        :return:
        """
        request = RequestUtil()
        url = host + "/pub/api/v1/web/web_login"
        data = {"phone": "19994334819", "pwd": "gao@367985"}
        headers = {"Content-type": "application/x-www-form-urlencoded"}
        response = request.request(url, 'post', param=data, headers=headers)
        self.assertEqual(response['code'], 0, "登录接口测试失败")


if __name__ == '__main__':
    unittest.main()
